package lab2.products;

public class Book extends Product {

	private int pageNumber;

	public Book(String name, String description, int pageNumber) {
		super(name, description);
		this.pageNumber = pageNumber;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	@Override
	public int hashCode() {
		String toHash = "Book:" + name + ":" + description + ":" + pageNumber;
		return toHash.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (other instanceof Book) {
			return pageNumber == ((Book) other).pageNumber;
		}
		return false;
	}

}
