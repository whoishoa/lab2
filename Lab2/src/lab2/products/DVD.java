package lab2.products;

public class DVD extends Product {

	private int duration;

	public DVD(String name, String description, int duration) {
		super(name, description);
		this.duration = duration;
	}

	public int getDuration() {
		return duration;
	}

	@Override
	public int hashCode() {
		String toHash = "DVD:" + name + ":" + description + ":" + duration;
		return toHash.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (other instanceof DVD) {
			return duration == ((DVD) other).duration;
		}
		return false;
	}

}
