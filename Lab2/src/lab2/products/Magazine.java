package lab2.products;

public class Magazine extends Product {

	private int weekNumber;
	
	public Magazine(String name, String description, int weekNumber) {
		super(name, description);
		this.weekNumber = weekNumber;
	}
	
	public int getWeekNumber(){
		return weekNumber;
	}
	
	@Override
	public int hashCode() {
		String toHash = "Magazine:" + name + ":" + description + ":" + weekNumber;
		return toHash.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (other instanceof Magazine){
			return weekNumber == ((Magazine) other).weekNumber;
		}
		return false;
	}

}
