package lab2.bookstore;

import java.util.HashMap;
import java.util.Set;

import lab2.products.Product;

public class Backend implements IBackend {

	private HashMap<Product, ProductInformation> database = new HashMap<Product, ProductInformation>();
	private Store store;
	
	public Backend(Store store) {
		this.store = store;
	}
	
	public Backend() {
		store = null;
	}

	public Set<Product> getDatabaseProducts(){
		Set<Product> keys = database.keySet();
		return keys;
	}
	
	@Override
	public void addInformation(Product product, int price)
			throws BookstoreException {
		// TODO Auto-generated method stub
		if (database.containsKey(product)) {
			throw new BookstoreException(
					"This Product is already in the database");
		}
		database.put(product, new ProductInformation(product, price, 0));
	}

	@Override
	public void add(Product product) throws BookstoreException {
		// TODO Auto-generated method stub
		if (database.containsKey(product)) {
			ProductInformation PI = database.get(product);
			PI.setNumberOfItemsAvailable(PI.getNumberOfItemsAvailable() + 1);
		} else {
			throw new BookstoreException("That product does not exist");
		}
	}

	@Override
	public void remove(Product product) throws BookstoreException {
		// TODO Auto-generated method stub
		if (database.containsKey(product)) {
			ProductInformation PI = database.get(product);
			if (PI.getNumberOfItemsAvailable() <= 0) {
				throw new BookstoreException("That product is not available");
			} else {
				PI.setNumberOfItemsAvailable(PI.getNumberOfItemsAvailable() - 1);
			}
		} else {
			throw new BookstoreException("That product does not exist");
		}
	}

	@Override
	public ProductInformation lookup(Product product) {
		if (database.containsKey(product)) {
			return database.get(product);
		}
		return null;
	}

}