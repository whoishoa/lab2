package lab2.bookstore;
import lab2.products.Product;

public class ProductInformation {
	private Product product;
	private int price;
	private int numberOfItemsAvailable;

	public ProductInformation(Product product, int price,
			int numberOfItemsAvailable) {
		this.product = product;
		this.price = price;
		this.numberOfItemsAvailable = numberOfItemsAvailable;
	}

	public int getNumberOfItemsAvailable() {
		return numberOfItemsAvailable;
	}

	public void setNumberOfItemsAvailable(int numberOfItemsAvailable) {
		this.numberOfItemsAvailable = numberOfItemsAvailable;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public Product getProduct(){
		return product;
	}

}
